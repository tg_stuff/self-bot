import asyncio
import logging
import toml
import traceback

from telethon import TelegramClient, events, types

CONFIG_PATH = 'config.toml'

config = toml.load(CONFIG_PATH)

logging.basicConfig(filename=f"self_bot.log", level=logging.INFO, format='%(asctime)s %(name)s - %(levelname)s:%(message)s')
logger = logging.getLogger(__name__)

def get_user_settings(phone):
  user = [user for user in config['users'] if phone in user['phone']][0]
  if not hasattr(user, 'command_sign'):
    user['command_sign'] = config['general']['command_sign']
  return user

async def filter_func(e):
  me = await e.client.get_me()
  my_settings = get_user_settings(me.phone)
  if e.out and e.reply_to is None and e.fwd_from is None and (e.is_channel or e.is_group):
    if e.message.media is None and e.message.message.startswith(my_settings['command_sign']):
      return True
  return False

async def check_user_admin(e):
  me = await e.client.get_me()
  permissions = await e.client.get_permissions(e.message.peer_id, me)
  if permissions.is_admin:
    return True
  else:
    return False

async def command_clean_this(e):
  # NB!: this delete either own messages (if not admin) or all messages (if admin)
  entity = await e.client.get_entity(e.message.peer_id)
  msg_ids = set()
  async for msg in e.client.iter_messages(entity, limit=None):
    msg_ids.add(msg.id)
  await e.client.delete_messages(entity, list(msg_ids))

async def command_clean_tags(e):
  # NB!: this delete either own messages (if not admin) or all messages (if admin)
  me = await e.client.get_me()
  my_settings = get_user_settings(me.phone)
  entity = await e.client.get_entity(e.message.peer_id)
  msg_ids = set()
  async for msg in e.client.iter_messages(entity, limit=None):
    if msg is not None and hasattr(msg, 'message') and hasattr(msg, 'entities') and msg.entities:
      for m_entity, m_entity_text in msg.get_entities_text():
        if type(m_entity) == types.MessageEntityHashtag:
          if m_entity_text.lower() in my_settings['clean_tags']:
            msg_ids.add(msg.id)
            break
  if msg_ids:
    await e.client.delete_messages(entity, list(msg_ids))
  await e.delete()

async def handler(e):
  try:
    me = await e.client.get_me()
    my_settings = get_user_settings(me.phone)
    if e.message.message == f'{my_settings["command_sign"]}clean_this':
      await command_clean_this(e)
    if e.message.message == f'{my_settings["command_sign"]}clean_tags':
      await command_clean_tags(e)
    # if e.message.message == f'{my_settings["command_sign"]}debug':
    #   await command_debug(e)
  except:
    logger.error(f'unhandled:\n{traceback.format_exc()}')

def main():
  for user in config['users']:
    client = TelegramClient(f'session_{user["phone"]}', user['api_id'], user['api_hash'])
    client.add_event_handler(handler, events.NewMessage(func=filter_func))
    client.start(user['phone'], user['password'])

  asyncio.get_event_loop().run_forever()

if __name__ == "__main__":
  main()
